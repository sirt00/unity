﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollideWithEnemy : MonoBehaviour
{

    void Start()
    {
        
    }

    void FixedUpdate()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PlayerBullet"))
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
 
}
